const getSum = (str1, str2) => {
  let output = '';

  if (typeof str1 !== 'string' || typeof str2 !== 'string')
    return false;

  for (let i = 0; i < str1.length; i++) {
    if (str1.charCodeAt(i) < 48 || str1.charCodeAt(i) > 57) {
      return false;
    }
  }

  for (let i = 0; i < str2.length; i++) {
    if (str2.charCodeAt(i) < 48 || str2.charCodeAt(i) > 57) {
      return false;
    }
  }

  let lenMax = str1.length >= str2.length ? str1.length : str2.length;
  let strMax = str1.length >= str2.length ? str1 : str2;

  let lenMin = str1.length < str2.length ? str1.length : str2.length;
  let strMin = str1.length < str2.length ? str1 : str2;

  let rem = false;
  for (let i = lenMin - 1, j = lenMax - 1; i >= 0; i--, j--) {
    let s = parseInt(strMin[i]) + parseInt(strMax[j]);
    s = rem === true ? s + 1 : s;

    if (s < 10) {
      rem = false;
    }
    else {
      rem = true;
      s -= 10;
    }
    output = s + output;
  }

  for (let j = lenMax - lenMin - 1; j >= 0; j--) {
    if (rem === true) {
      let k = parseInt(strMax[j]) + 1;
      output = k + output;
      rem = false;
    }
    output = strMax[j] + output;
  }

  return output;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = 0;
  let commentCount = 0;

  for (const listOfPost of listOfPosts) {

    if (listOfPost.author === authorName) {
      postCount++;
    }
  }

  for (const listOfPost of listOfPosts) {
    if (listOfPost.comments){
      for (const comments of listOfPost.comments) {
        if (comments.author === authorName) {
          commentCount++;
        }
      }
    }
  }

  return `Post:${postCount},comments:${commentCount}`;
};

const tickets=(people)=> {
  let cash = 0;
  let ticketCost = 25;

  for (const bill of people) {
    if (bill !== 25 && bill !== 50 && bill !== 100) {
      return "NO";
    }
    if (bill === 50 && cash < 25) {
        return "NO";
    }
    if (bill === 100 && cash < 75) {
        return "NO";
    }

    cash += ticketCost;
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
